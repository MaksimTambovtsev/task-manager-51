package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;

public interface IRepository<M extends AbstractEntity> {

    @Nullable
    M findById(@Nullable String id);

    void removeById(@Nullable String id);

    void removeCascade(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    void create(@NotNull M model);

    void update(@NotNull M model);

}
