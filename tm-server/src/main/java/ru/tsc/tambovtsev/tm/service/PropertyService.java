package ru.tsc.tambovtsev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "12345";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "6363453453";

    @NotNull
    private static final String SESSION_TYMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TYMEOUT_DEFAULT = "86400";

    @NotNull
    private static final String DATABASE_USERNAME = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "root";

    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "sadmin";

    @NotNull
    private static final String DATABASE_URL = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT =
            "jdbc:mysql://localhost:3306/example?createDatabaseIfNotExist=true";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "com.mysql.cj.jdbc.Driver";

    @NotNull
    private static final String DATABASE_SQL_DIALECT = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_DEFAULT = "org.hibernate.dialect.MySQL5InnoDBDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CASH = "database.second_lvl_cash";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CASH_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS_DEFAULT =
            "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH = "database.use_query_cash";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace('.', '_').toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull String value = getStringValue(key, defaultValue);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    public @NotNull Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TYMEOUT_KEY, SESSION_TYMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return properties.getProperty(DATABASE_USERNAME, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return properties.getProperty(DATABASE_PASSWORD, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return properties.getProperty(DATABASE_URL, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return properties.getProperty(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getSQLDialect() {
        return properties.getProperty(DATABASE_SQL_DIALECT, DATABASE_SQL_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getHbm2ddlAuto() {
        return properties.getProperty(DATABASE_HBM2DDL_AUTO, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getShowSql() {
        return properties.getProperty(DATABASE_SHOW_SQL, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getFormatSql() {
        return properties.getProperty(DATABASE_FORMAT_SQL, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @Override
    public @NotNull String getSecondLvlCash() {
        return properties.getProperty(DATABASE_SECOND_LVL_CASH, DATABASE_SECOND_LVL_CASH_DEFAULT);
    }

    @Override
    public @NotNull String getFactoryClass() {
        return properties.getProperty(DATABASE_FACTORY_CLASS, DATABASE_FACTORY_CLASS_DEFAULT);
    }

    @Override
    public @NotNull String getUseQueryCash() {
        return properties.getProperty(DATABASE_USE_QUERY_CASH, DATABASE_USE_QUERY_CASH_DEFAULT);
    }

    @Override
    public @NotNull String getUseMinPuts() {
        return properties.getProperty(DATABASE_USE_MIN_PUTS, DATABASE_USE_MIN_PUTS_DEFAULT);
    }

    @Override
    public @NotNull String getRegionPrefix() {
        return properties.getProperty(DATABASE_REGION_PREFIX, DATABASE_REGION_PREFIX_DEFAULT);
    }

    @Override
    public @NotNull String getConfigFilePath() {
        return properties.getProperty(DATABASE_CONFIG_FILE_PATH, DATABASE_CONFIG_FILE_PATH_DEFAULT);
    }

}
