package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.model.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    @Nullable
    private final IConnectionService connectionService;

    public ProjectTaskService(
            @Nullable final IProjectRepository projectRepository,
            @Nullable final ITaskRepository taskRepository,
            @Nullable IConnectionService connectionService
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (task == null) return;
            entityManager.getTransaction().begin();
            task.setProject(project);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            entityManager.getTransaction().begin();
            task.setProject(null);
            repository.updateById(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            @Nullable final Project project = projectRepository.findById(userId, projectId);
            if (project == null) return;
            entityManager.getTransaction().begin();
            projectRepository.removeCascade(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
