package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory getHibernateFactory();

    @NotNull
    EntityManager getEntityManager();

    void close();

}
