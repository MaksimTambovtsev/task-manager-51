package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.dto.TaskService;
import ru.tsc.tambovtsev.tm.service.dto.UserService;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.List;

public final class TaskServiceTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user = new UserDTO();

    @Nullable
    private TaskDTO task;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void disconnectConnection() {
        connectionService.close();
    }

    @Before
    public void setTaskService() {
        projectRepository = new ProjectRepository(connectionService.getEntityManager());
        taskRepository = new TaskRepository(connectionService.getEntityManager());
        userRepository = new UserRepository(connectionService.getEntityManager());
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        taskService = new TaskService(taskRepository, connectionService);
        @Nullable TaskDTO task = new TaskDTO();
        user.setEmail("test@test.ru");
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt(propertyService, "test"));
        userService.create(user);
        task.setUserId(user.getId());
        task.setName("123");
        task.setDescription("432");
        taskService.create(task);
        task = new TaskDTO();
        task.setUserId(user.getId());
        task.setName("1321");
        task.setDescription("234");
        taskService.create(task);
    }

    @After
    public void clearTaskService() {
        taskService.clearTask();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(taskService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(taskService.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        @Nullable final String taskId = tasks.stream().findFirst().get().getId();
        Assert.assertFalse(taskService.findById(taskId).getName().isEmpty());
    }

    @Test
    public void testRemove() {
        @NotNull final TaskDTO task = taskService.findAll().get(1);
        taskService.removeById(task.getId());
        Assert.assertNotEquals(task.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        @NotNull final TaskDTO task = taskService.findAll().get(1);
        taskService.removeById(task.getId());
        Assert.assertNotEquals(task.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void testUpdateById() {
        @NotNull final TaskDTO task = taskService.findAll().get(0);
        taskService.updateById(task.getUserId(), task.getId(), "Project1", "Descriptions");
        Assert.assertEquals(taskService.findById(task.getId()).getName(), "Project1");
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdNegative() {
        @NotNull final TaskDTO task = taskService.findAll().get(0);
        taskService.updateById(task.getUserId(), null, "Project1", "Descriptions");
        Assert.assertEquals(taskService.findById(task.getId()).getName(), "Project1");
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskDTO task = taskService.findAll().get(0);
        taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
        Assert.assertEquals(taskService.findById(task.getId()).getStatus(), Status.COMPLETED);
    }

    @Test
    public void testClear() {
        taskService.clearTask();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

}
