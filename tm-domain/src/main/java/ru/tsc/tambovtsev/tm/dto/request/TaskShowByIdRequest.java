package ru.tsc.tambovtsev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskShowByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
